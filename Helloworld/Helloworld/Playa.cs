﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Helloworld
{
    class Playa
    {
        public Model Model { get; set; }
        public Vector2 Position { get; set; }
        public bool IsActive { get; set; }
        public BoundingSphere BoundingSphere { get; set; }

        public Playa()
        {
            Model = null;
            Position = Vector2.Zero;
            IsActive = false;
            BoundingSphere = new BoundingSphere();
        }
    }
}
