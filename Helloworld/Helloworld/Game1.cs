using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Helloworld
{

    public class Game1 : Microsoft.Xna.Framework.Game
    {

        GraphicsDevice device;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spriteFont;

        Texture2D myTex;
        Texture2D myTex1;
        Texture2D myTex2;
        Texture2D terrain;
        Texture2D foregroundTex;
        Texture2D backgroundTex;

        Vector2 spritePosi = Vector2.Zero;
        Vector2 spriteSpeed = Vector2.Zero;

        Vector2 speedMin = new Vector2(1f, 1f);
        GamePadState lastGamePadState = new GamePadState();
        GamePadState currentGamePadState = new GamePadState();
        KeyboardState lastKeyboardState = new KeyboardState();
        KeyboardState currentKeyboardState = new KeyboardState();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            graphics.PreferredBackBufferWidth = GameConstants.screenWidth;
            graphics.PreferredBackBufferHeight = GameConstants.screenHeight;
            graphics.IsFullScreen = GameConstants.fullScreen;
            graphics.ApplyChanges();
            Window.Title = GameConstants.gameTitle;
            base.Initialize();
        }

        protected override void LoadContent()
        {
            device = graphics.GraphicsDevice;

            spriteFont = Content.Load<SpriteFont>("SpriteFont1");
            backgroundTex = Content.Load<Texture2D>("first_background");

            spriteBatch = new SpriteBatch(GraphicsDevice);
            
            //hack to change direction of sprite
            myTex = Content.Load<Texture2D>("beep_sprite_01");
        }

        protected override void UnloadContent()
        {
        }


        double vi = 2220, t = 0;
        double g = 2220;
        Vector2 startPosi = Vector2.Zero;
        bool airborn = true;
        bool freefall = true;
        bool reverse = false;
        //runs @ 60fps
        protected override void Update(GameTime gameTime)
        {
            lastKeyboardState = currentKeyboardState;
            lastGamePadState = currentGamePadState;

            currentKeyboardState = Keyboard.GetState();
            currentGamePadState = GamePad.GetState(PlayerIndex.One);

            if (airborn == true)
            {
                if (spriteSpeed.Y < GameConstants.terminalVelocity)
                    spriteSpeed.Y += GameConstants.gravity;
            }


            if ((currentKeyboardState.IsKeyDown(Keys.A) || currentGamePadState.IsButtonDown(Buttons.A)) && spriteSpeed.Y > -GameConstants.terminalVelocity && !freefall)
            {
                spriteSpeed.Y -= GameConstants.jumpInterval;
                airborn = true;
            }
            if (spriteSpeed.Y <= -GameConstants.terminalVelocity)
                freefall = true;

            if (currentKeyboardState.IsKeyDown(Keys.Left) && spriteSpeed.X > -GameConstants.speedLimit)
            {
                spriteSpeed.X -= GameConstants.speedInterval;
                reverse = true;
            }
            else if (spriteSpeed.X < 0f) 
            {
                spriteSpeed.X += GameConstants.speedInterval;
            }

            if (currentKeyboardState.IsKeyDown(Keys.Right) && spriteSpeed.X < GameConstants.speedLimit)
            {
                spriteSpeed.X += GameConstants.speedInterval;
               // myTex = myTex2;
                reverse = false;
            }
            else if (spriteSpeed.X > 0f)
            {
                spriteSpeed.X -= GameConstants.speedInterval;
            }

            if (currentKeyboardState.IsKeyDown(Keys.Down))
            {
                spritePosi.Y -= spriteSpeed.Y;
            }

            if (currentGamePadState.ThumbSticks.Left.Length() > 0)
            {
                spritePosi += currentGamePadState.ThumbSticks.Left * spriteSpeed;
                if (currentGamePadState.ThumbSticks.Left.X > 0)
                {
                    //myTex = myTex2;
                }
                else
                {
                    //myTex = myTex1;
                }
            }
            else
            {
                spritePosi += Vector2.Zero;
            }

            //spritePosi += spriteSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            int maxX = graphics.GraphicsDevice.Viewport.Width - myTex.Width;
            int minX = 0;
            int maxY = graphics.GraphicsDevice.Viewport.Height - myTex.Height;
            int minY = 0;

            if (spritePosi.X > maxX)
            {
               // spriteSpeed.X *= 0;
                spritePosi.X = maxX;
            }

            else if (spritePosi.X < minX)
            {
              //  spriteSpeed.X *= 0;
                spritePosi.X = minX;
            }

            if(spritePosi.Y > maxY)
            {
                spriteSpeed.Y *= 0;
                spritePosi.Y = maxY;
                airborn = false;
                freefall = false;
            }
            else if (spritePosi.Y < minY)
            {
                spritePosi.Y = minY;
                spriteSpeed.Y = 0;
            }

            spritePosi += spriteSpeed;

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || currentKeyboardState.IsKeyDown(Keys.Escape))
                this.Exit();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself. (as many times as possible)
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            // Draw my silly sprite
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
            //spriteBatch.Draw(myTex, spritePosi, Color.White);
            if(reverse == true)
                spriteBatch.Draw(myTex, spritePosi, null, Color.White, 0, Vector2.Zero, 1f, SpriteEffects.FlipHorizontally, 0);
            else
                spriteBatch.Draw(myTex, spritePosi, null, Color.White, 0, Vector2.Zero, 1f, SpriteEffects.None, 0);
            DrawScenery();
            DrawText();
            spriteBatch.End();

            base.Draw(gameTime);
        }
        private void DrawScenery()
        {
            Rectangle screenRect = new Rectangle(0, 0, GameConstants.screenWidth, GameConstants.screenHeight);
            spriteBatch.Draw(backgroundTex, screenRect, Color.White);
        }

        private void DrawText()
        {
            spriteBatch.DrawString(spriteFont, "Life: 100", new Vector2(0, 0), Color.White);
        }
    }
}
