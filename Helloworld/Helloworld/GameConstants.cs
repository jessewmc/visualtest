﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Helloworld
{
    class GameConstants
    {
        public const int screenWidth = 1280;
        public const int screenHeight = 720;
        public const string gameTitle = "Machine";
        public const float gravity = 2f;
        public const float speedInterval = 5f;
        public const float jumpInterval = 5f;
        public const float terminalVelocity = 20f;
        public const bool fullScreen = false;
        public const float speedLimit = 12f;
    }
}
